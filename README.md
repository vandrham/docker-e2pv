# Usage

```
docker run -d \
  -v $(pwd)/config:/app/config \
  --net=host vandrham/e2pv
```


This will start a container and copy a default configuration file into the `/config` folder.

You can edit the config to suit your needs. See https://github.com/omoerbeek/e2pv for more details.