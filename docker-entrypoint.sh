#!/bin/bash

if [ ! -f /app/config/config.php ]
then
  cp /app/config.php.default /app/config/config.php
fi

php -f /app/e2pv.php