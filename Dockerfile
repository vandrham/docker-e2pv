FROM php:7-cli

RUN docker-php-ext-install sockets

COPY e2pv /app
COPY docker-entrypoint.sh /
COPY e2pv.php.ini $PHP_INI_DIR/conf.d

EXPOSE 5040

ENTRYPOINT ["/docker-entrypoint.sh"]